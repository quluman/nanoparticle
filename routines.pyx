
#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False


from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge
from event_class cimport Hopping, s_Hopping

import numpy as np
cimport numpy as np 

#cimport electron_library as el
#cimport hole_library as hl
cimport utility as util
#import utility as putil

cimport constants as c

#import time 

#import matplotlib.pyplot as plt

from libc.math cimport log
from libc.math cimport fabs



cdef np.ndarray set_nanoparticles(Sample sample):
    
    
    cdef:
        str filename ='input/nanoparticles/'+ str(sample.nnanops)+'/' + 'nanoparticles'+str(sample.sample_number)+'.inp'    
        list temp
    
    #print('opening sample number ', x)
    
    #f = open('input/nanoparticle.inp', 'r')
    f = open(filename, 'r')
    
    #read degeneracy information
    
    f.readline()
    temp = ((f.readline()).strip()).split()
    #print(temp.split())
    sample.e_degeneracy = int(temp[0])
    sample.h_degeneracy = int(temp[1])
    f.readline()
    
    temp = ((f.readline()).strip()).split(",")
    #print(temp[1])
    sample.cellx = float(temp[1]) * c.nmtobohr
    
    temp = ((f.readline()).strip()).split(",")
    #print(temp[1])
    sample.celly = float(temp[1]) * c.nmtobohr
    
    temp = ((f.readline()).strip()).split(",")
    #print(temp[1])
    sample.cellz = float(temp[1]) * c.nmtobohr
    sample.cellz_nm = float(temp[1])
    
    
    sample.beta = sample.beta/(sample.cellx*sample.celly*2.0*sample.cellz)**(1.0/3.0)
    

    cdef np.ndarray nanoparticles = np.empty(sample.nnanops, dtype = Nanoparticle)
    
    #cdef pc.nanoparticle[:] nanoparticles 
    
    cdef INT_t i=0
    cdef INT_t j=0
    
    cdef INT_t nbnd = sample.nbnd
    cdef INT_t degeneracy = sample.e_degeneracy
    
   
    cdef:
        np.ndarray[INT_t] orboccupation
        np.ndarray[INT_t] orbmaxoccupation
        np.ndarray[INT_t] vborboccupation
        np.ndarray[INT_t] vborbmaxoccupation
        np.ndarray[INT_t] electronindex
        np.ndarray[INT_t] holeindex
        np.ndarray levels
        
        str line
        list columns

   
    for line in f:
       
        line = line.strip()
        columns = line.split(",")
        particle = Nanoparticle()
        particle.x = float(columns[0])*c.nmtobohr
        particle.y = float(columns[1])*c.nmtobohr
        particle.z = float(columns[2])*c.nmtobohr
        particle.diameter = float(columns[3])*c.nmtobohr - 2.0*sample.ligandlength  #diam(i)*nmtobohr-2.0_DP*ligandlength
        particle.radius = particle.diameter / 2.0
        particle.set_cbenergy1()
        particle.set_cbenergy2()
        particle.set_vbenergy()

        
        orboccupation = np.zeros(nbnd, dtype = np.int)
        vborboccupation = np.zeros(nbnd, dtype = np.int)
        orbmaxoccupation = np.zeros(nbnd, dtype = np.int)
        vborbmaxoccupation = np.zeros(nbnd, dtype = np.int)
        #electronindex = np.zeros(nbnd, dtype = np.int)
        #holeindex = np.zeros(nbnd, dtype = np.int)
        
        
        for j in range(nbnd):
            orbmaxoccupation[j] = degeneracy
            vborbmaxoccupation[j] = degeneracy
        
        # assign correct electrons and holes slots in each orbital
        #electronindex.resize(orbmaxoccupation.sum())
        electronindex = np.zeros(orbmaxoccupation.sum(), dtype = np.int)
        electronindex.fill(-1)

        
        #holeindex.resize(orbmaxoccupation.sum())
        holeindex = np.zeros(orbmaxoccupation.sum(), dtype = np.int)
        holeindex.fill(-1)
           
        
        particle.orboccupation = orboccupation
        particle.orbmaxoccupation = orbmaxoccupation
        particle.vborboccupation = vborboccupation
        particle.vborbmaxoccupation = vborbmaxoccupation
        particle.electronindex = electronindex
        particle.holeindex = holeindex
                
        nanoparticles[i] = particle

        i = i+1
 
        #config[columns[0]] = read_float(columns[2])
        
    f.close()
    
    levels = np.asarray([util.get_levels(nanoparticles)])
    
    sample.FWHM = util.fit_gaussian(levels, 50, False)   # needs more inspection, about the range of the fitting

    return nanoparticles


cdef np.ndarray set_electrons(Sample sample):

    cdef np.ndarray electrons = np.empty(sample.nelec, dtype = Charge)  
    cdef INT_t i
        
    for i in range(sample.nelec):
        elec = Charge()
        elec.charge = -1.0*c.sqrt2
        elec.mass = sample.emass
        electrons[i] = elec
    
    return electrons

cdef np.ndarray set_holes(Sample sample):
    
    cdef np.ndarray holes = np.empty(sample.nhole, dtype = Charge)
    cdef INT_t i

    for i in range(sample.nhole):
        hole = Charge()
        hole.charge = 1.0*c.sqrt2
        hole.mass = sample.hmass
        holes[i] = hole
    
    return holes



cdef s_Charge set_s_electrons(s_Charge[:] elec, Sample sample):
    
    cdef INT_t i
    
    for i in range(sample.nelec):
        elec[i].charge = -1.0*c.sqrt2
        elec[i].mass = sample.emass


cdef s_Charge set_s_holes(s_Charge[:] hole, Sample sample):
    
    cdef INT_t i
    
    for i in range(sample.nhole):
        hole[i].charge = 1.0*c.sqrt2
        hole[i].mass = sample.hmass






cdef dielectrics(Nanoparticle[:] nanops, Sample sample):
    
    cdef: 
        FLOAT_t packingfraction = 0.0
        FLOAT_t npvolume = 0.0
        FLOAT_t dcout = 0.0
        Nanoparticle nanop
    
    # Set dcin for each nanoparticle
    for nanop in nanops:
        if (sample.pennmodel):
            nanop.dcin = 1+(sample.npdc-1)/(1+(sample.bradii/nanop.diameter)**2)
        else:
            nanop.dcin = sample.npdc
    
    # Set dcout fir the sample 
    if (sample.lemmg):
        for nanop in nanops:
            npvolume = npvolume + 4.0 /3.0 * nanop.radius**3*c.pi
        packingfraction = npvolume / (sample.cellx*sample.celly*sample.cellz)
        dcout = sample.liganddc*(sample.npdc*(1+2*packingfraction)-sample.liganddc*(2*packingfraction-2))/(sample.liganddc*(2+packingfraction)+sample.npdc*(1-packingfraction))
      
    elif (sample.lemla):
        for nanop in nanops:
            npvolume = npvolume + 4.0/3.0*nanop.radius**3*c.pi
        packingfraction=npvolume/(sample.cellx*sample.celly*sample.cellz)
        dcout=(1-packingfraction)*sample.liganddc+packingfraction*sample.npdc     
     
    elif (sample.lemlll):
        print('running here')
        for nanop in nanops:
            npvolume = npvolume + 4.0 / 3.0 *nanop.radius**3*c.pi
        packingfraction = npvolume/(sample.cellx*sample.celly*sample.cellz)
        dcout = ((1-packingfraction)*sample.liganddc**(1.0 /3.0 )+packingfraction*sample.npdc**(1.0 /3.0 ))**3
  
    elif (sample.lempnone):
        print('running here xx')
        dcout = sample.liganddc

    sample.dcout = dcout
    sample.packingfraction = packingfraction
    
    




cdef INT_t neighborlist(Nanoparticle[:] nanops, Sample sample):
    cdef INT_t i,j,k,l,m, totalnn, totalcn
    
    # check for number of neighbors and then allocate neighbor index array
   
    cdef INT_t number = nanops.shape[0]
    cdef FLOAT_t dist_thr, ediff_thr
    
    dist_thr = sample.ndist_thr
    ediff_thr = sample.ediff_thr
    
    totalnn = 0
    totalcn = 0
        
    for i in range(number):
        for j in range(number):
            if (i != j):
                if (util.npnpdistance(nanops[i],nanops[j],sample) <= dist_thr):                    
                    nanops[i].nn = nanops[i].nn + 1
##### Works only for one band only here!!!                    
                    #if (fabs((nanops[i].cbenergy[0]-nanops[j].cbenergy[0])/(nanops[i].cbenergy[0])) < ediff_thr):  OLD version
                    if (fabs(nanops[i].cbenergy[0]-nanops[j].cbenergy[0]) < ediff_thr):
                        nanops[i].cn = nanops[i].cn + 1

                    #if (fabs((nanops[i].vbenergy[0]-nanops[j].vbenergy[0])/(nanops[i].vbenergy[0])) < ediff_thr): OLD version
                    if (fabs(nanops[i].vbenergy[0]-nanops[j].vbenergy[0]) < ediff_thr):
                        nanops[i].hcn = nanops[i].hcn + 1

    for i in range(number):
        #nanops[i].nnindex.resize(nanops[i].nn)
        # Now nanops are really memoview slice not numpy ndarray
        nanops[i].nnindex = np.zeros(nanops[i].nn, dtype = np.int)
        
        nanops[i].if_cn = np.zeros(nanops[i].nn, dtype = np.int)   
        nanops[i].if_hcn = np.zeros(nanops[i].nn, dtype = np.int)
        
        nanops[i].cnindex = np.zeros(nanops[i].cn, dtype = np.int)
        nanops[i].hcnindex = np.zeros(nanops[i].hcn, dtype = np.int)
        
        totalnn += nanops[i].nn
        totalcn += nanops[i].cn
    # make neighbor index array 
    #
    for i in range(number):
        k = 0  # nn index
        l = 0  # cn index
        m = 0  # hcn index

        for j in range(number):

            if (i != j):

                if (util.npnpdistance(nanops[i],nanops[j],sample) <= dist_thr):
                    
                    nanops[i].nnindex[k] = j
                    k=k+1
                    
                    #if (fabs((nanops[i].cbenergy[0]-nanops[j].cbenergy[0])/(nanops[i].cbenergy[0])) < ediff_thr):  OLD version
                    if (fabs(nanops[i].cbenergy[0]-nanops[j].cbenergy[0]) < ediff_thr):
                        nanops[i].cnindex[l] = j
                        l=l+1
                        nanops[i].if_cn[k-1] = 1  # meaning the k th element in the nnindex array is the close neighbor

                    #if (fabs((nanops[i].vbenergy[0]-nanops[j].vbenergy[0])/(nanops[i].vbenergy[0])) < ediff_thr):  OLD version
                    if (fabs(nanops[i].vbenergy[0]-nanops[j].vbenergy[0]) < ediff_thr):
                        nanops[i].hcnindex[m] = j
                        m=m+1
                        nanops[i].if_hcn[k-1] = 1
    
    #print('totalnn ', totalnn)
    #print('totalcn ', totalcn)
    
    return 0

#cdef int find_electrode(np.ndarray['object'] nanops, pc.electrode sources, pc.electrode drains, double dist_thr):
cdef INT_t find_electrode(Nanoparticle[:] nanops, Electrode sources, Electrode drains, Sample sample):
    
    # yet only for spheres
    cdef INT_t i 
    cdef INT_t number = int(nanops.shape[0])
    cdef FLOAT_t cellz, dist_thr
    
    dist_thr = sample.ndist_thr    
    cellz = sample.cellz
    
    
    sources.nn = 0 
    for i in range(number):
      if ((nanops[i].z-nanops[i].radius) <= dist_thr):
          sources.nn = sources.nn + 1

    drains.nn = 0
    for i in range(number):
      if ((cellz-(nanops[i].z+nanops[i].radius)) <= dist_thr):
          drains.nn = drains.nn + 1
      
    # z=0 plane is where electrons flow into the system
    # z=L plane is where electrons are collected

    sources.nnindex = np.zeros(sources.nn, dtype = np.int)
    drains.nnindex = np.zeros(drains.nn, dtype = np.int)

    sources.nn = 0
    for i in range(number):
      if (( nanops[i].z - nanops[i].radius ) <= dist_thr):
          sources.nnindex[sources.nn] = i
          nanops[i].source = True
          sources.nn = sources.nn + 1

    drains.nn = 0
    for i in range(number):    
        if ((cellz-(nanops[i].z+nanops[i].radius)) <= dist_thr):
            drains.nnindex[drains.nn] = i
            nanops[i].drain = True
            drains.nn = drains.nn + 1
            
    return 0



cdef list initialize_events(Nanoparticle[:] nanops, Sample sample, Charge[:] electrons, Charge[:] holes):
    cdef: 
        INT_t totalnn = 0       # Total number of nearest-neighbor
        INT_t totalevents = 0   # Total number of possible events
        INT_t eventsize = 0  # effective event size
        INT_t heventsize = 0
        Nanoparticle nanop
    
    for nanop in nanops:
        totalnn += nanop.nn
    
    totalevents = totalnn * sample.e_degeneracy * 2  * sample.nbnd

    eventsize = int(totalevents*electrons.shape[0]/5000)
    
    heventsize = int(totalevents*holes.shape[0]/5000)

    # Initialize hopping events !!!!   
    
    cdef np.ndarray[object] elechopping = np.empty(eventsize, dtype = Hopping)
    cdef np.ndarray[object] holehopping = np.empty(heventsize, dtype = Hopping)    
    
    # buffer size is 1000 here
    
    cdef s_Hopping s_elechopping[1000]
    cdef s_Hopping s_holehopping[1000]
    
    
    for i in range(eventsize):      
        elechopping[i] = Hopping(0)
        s_elechopping[i].hopping_type = 0

    for i in range(heventsize):        
        holehopping[i] = Hopping(1)
        s_holehopping[i].hopping_type = 1
    

    return [elechopping, holehopping]


#######cdef ec.hopping_event linearsearch(ec.hopping_event[:] hopping, ec.hopping_event[:] hhopping, FLOAT_t[:] rand, INT_t nevents, INT_t nhevents):
#cdef Hopping linearsearch(Hopping[:] hopping, Hopping[:] hhopping, INT_t nevents, INT_t nhevents):
cdef s_Hopping linearsearch(s_Hopping[:] hopping, s_Hopping[:] hhopping, INT_t nevents, INT_t nhevents):
    #cdef INT_t nevents, nhevents
    cdef:
        INT_t i
        FLOAT_t sumrate, r, sums
    
    sumrate = 0
    
    #nevents = hopping.shape[0]
    #nhevents = hhopping.shape[0]    
    #print('nevents', nevents)
    #print('nhevents',nhevents)

    for i in range(nevents):
        sumrate += hopping[i].rate
    for i in range(nhevents):
        sumrate += hhopping[i].rate

    #r = sumrate * np.random.random()
    r = sumrate * util.mt_ldrand()
    
    sums = 0
    i = 0
    #elechopping = False

    while (r >= sums):
        
        if (i < nevents):
            sums += hopping[i].rate
            #elechopping = True

        elif ( (i == nevents) & (nhevents==0) ):
            break
        
        else:            
            sums += hhopping[i-nevents].rate    # Touble here!!
            #elechopping = False
        i = i+1         
    
    if i <= nevents:
        hopping[i-1].totalrate = sumrate
        return hopping[i-1]
    
    else:
        hhopping[i-nevents-1].totalrate = sumrate
        return hhopping[i-nevents-1]
    

cdef FLOAT_t get_sumrate(Hopping[:] hopping, Hopping[:] hhopping):
    cdef:
        INT_t nevents, nhevents, i
        FLOAT_t sumrate
    
    sumrate = 0
    
    nevents = hopping.shape[0]
    nhevents = hhopping.shape[0]

    for i in range(nevents):
        sumrate += hopping[i].rate
    for i in range(nhevents):
        sumrate += hhopping[i].rate

    return sumrate    




cdef Hopping numpysearch(Hopping[:] hopping, Hopping[:] hhopping, INT_t nevents, INT_t nhevents, FLOAT_t[:] probability):
    #cdef INT_t nevents, nhevents
    cdef:
        INT_t i, select
        FLOAT_t sumrate
    
    sumrate = 0
    
    for i in range(nevents):
        probability[i] = hopping[i].rate
        
    for i in range(nhevents):
        probability[i+nevents] = hhopping[i].rate
        
    
    
    for i in range(nevents+nhevents):
        sumrate += probability[i] 
    
    for i in range(nevents+nhevents):
        probability[i] = probability[i] / sumrate
    
    select = np.random.choice(probability.shape[0], p = probability)

    
    if select < nevents:
        hopping[select].totalrate = sumrate
        return hopping[select]
    
    else:
        hhopping[select-nevents].totalrate = sumrate
        return hhopping[select-nevents]



cdef Hopping numpysearch_fast(Hopping[:] hopping, Hopping[:] hhopping, INT_t nevents, INT_t nhevents, FLOAT_t[:] rates):
    #cdef INT_t nevents, nhevents
    cdef:
        INT_t i, select
        FLOAT_t sumrate
    
    sumrate = 0
    
    for i in range(rates.shape[0]):
        sumrate += rates[i]
    
    # Normalize the rates for probability
    for i in range(rates.shape[0]):
        rates[i] = rates[i] / sumrate  
    
    select = np.random.choice(rates.shape[0], p = rates)

    
    if select < nevents:
        hopping[select].totalrate = sumrate
        return hopping[select]
    
    else:
        hhopping[select-nevents].totalrate = sumrate
        return hhopping[select-nevents]

"""
cdef Hopping numpysearch_fast(Hopping[:] hopping, Hopping[:] hhopping, INT_t nevents, INT_t nhevents, np.ndarray[FLOAT_t] rates):
    #cdef INT_t nevents, nhevents
    cdef:
        INT_t i, select
        FLOAT_t sumrate
    
    sumrate = rates.sum()
    
    # Normalize the rates for probability
    rates = rates / sumrate  
    
    select = np.random.choice(rates.shape[0], p = rates)

    
    if select < nevents:
        hopping[select].totalrate = sumrate
        return hopping[select]
    
    else:
        hhopping[select-nevents].totalrate = sumrate
        return hhopping[select-nevents]

"""