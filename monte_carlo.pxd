#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False

from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge

from event_class cimport Hopping, s_Hopping

from electron_library cimport throw_electron, find_events_cluster, execute_event

from hole_library cimport throw_hole, find_hole_events_cluster, execute_hole_event

cimport routines 

from routines cimport linearsearch, numpysearch, numpysearch_fast

cimport utility as util
cimport constants as c

import numpy as np
cimport numpy as np
import time

from libc.math cimport log, fabs

ctypedef np.float64_t FLOAT_t
ctypedef np.int_t INT_t

cpdef monte_carlo(INT_t steps, INT_t sample_no, INT_t e_number, INT_t h_number, INT_t nanops_number, FLOAT_t temp, FLOAT_t thr, str features, output)
    

 