#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False

cimport constants as c
from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge
from event_class cimport Hopping, s_Hopping
cimport utility as util
import numpy as np
cimport numpy as np
from libc.math cimport sqrt, fmin
from libc.math cimport fabs
from libc.math cimport exp
from libc.math cimport round

ctypedef np.float64_t FLOAT_t
ctypedef np.int_t INT_t




cdef INT_t throw_hole(Nanoparticle[:] nanops, s_Charge[:] holes, Sample sample)
    
cdef INT_t find_hole_events_size(Nanoparticle[:] nanops, Charge[:] holes, Sample sample)
    
cdef INT_t find_hole_events(Nanoparticle[:] nanops, Charge[:] holes, Hopping[:] holehoppings, Sample sample)

cdef INT_t execute_hole_event(Nanoparticle[:] nanops, s_Charge[:] holes, s_Hopping executedhopping)
  

### Funtions used in this part
 
   
cdef FLOAT_t electronholeeh(Nanoparticle[:] nanops, s_Hopping hopping)
  
cdef FLOAT_t get_hrate(Nanoparticle[:] nanops, s_Hopping hopping, Sample sample)

cdef INT_t findholenewstate(Nanoparticle[:] nanop, INT_t p, INT_t o)

cdef INT_t find_hole_events_cluster(Nanoparticle[:] nanops, s_Charge[:] holes, s_Hopping[:] holehoppings, Sample sample)



