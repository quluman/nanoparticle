
#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False



from particle_class cimport Sample, Nanoparticle, Electrode, Charge, Cluster
from event_class cimport Hopping, s_Hopping

cimport utility as util
cimport constants as c

import numpy as np
cimport numpy as np
import scipy.optimize as opt

import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D

import os

import vispy.scene
from vispy.scene import visuals
import vispy.io as io

from libc.math cimport log
from libc.math cimport fabs,fmin, sqrt
from libc.stdint cimport uint32_t


cpdef INT_t root(INT_t[:,:] idlist, INT_t n):
    while (n != idlist[n, 0]):
        n = idlist[n, 0]
    return n
        
cpdef INT_t root_zip(INT_t[:,:] idlist, INT_t n):           # root with path compression                 
    while (n != idlist[n, 0]):
        idlist[n,0] = idlist[idlist[n,0],0]
        n = idlist[n, 0]
    return n
        

cpdef bint connected(INT_t[:,:] idlist, INT_t p, INT_t q):
    return root(idlist, p) == root(idlist, q)
        

cpdef INT_t quickunion(INT_t[:,:] idlist, INT_t p, INT_t q):
    cdef INT_t m = root(idlist, p)
    cdef INT_t n = root(idlist, q)
    idlist[m,0] = n
    return 0
    
cpdef INT_t quickunion_w(INT_t[:,:] idlist, INT_t p, INT_t q):     # weighted Quick-Union
    cdef INT_t m = root_zip(idlist, p)
    cdef INT_t n = root_zip(idlist, q)    
    if (m==n):
        return 0
    if(idlist[m,1] < idlist[n,1]):
        idlist[m,0] = n
        idlist[n,1] += idlist[m,1]
        return 0
    else:
        idlist[n,0] = m
        idlist[m,1] += idlist[n,1] 
        return 0


cpdef INT_t visual_cluster(Nanoparticle[:] nanops, Cluster[:] clusterz):
    
    canvas = vispy.scene.SceneCanvas(keys='interactive', size=(600, 1200), show=True, bgcolor='white')
    view = canvas.central_widget.add_view()
    
    cdef INT_t i,j
    cdef np.ndarray pos, sz
    cdef INT_t nanoid    

    for i in range(clusterz.shape[0]):            
        pos = np.zeros([clusterz[i].npindex.shape[0],3])
        sz = np.zeros(clusterz[i].npindex.shape[0])
        for j in range(clusterz[i].npindex.shape[0]):            
            nanoid = clusterz[i].npindex[j]            
            pos[j,0] = nanops[nanoid].x
            pos[j,1] = nanops[nanoid].y
            pos[j,2] = nanops[nanoid].z
            sz[j] = nanops[nanoid].radius
        
        scatter = visuals.Markers()
        scatter.set_data(pos, edge_color=None, face_color=(np.random.rand(), np.random.rand(), np.random.rand(), 1), size=sz)
        #scatter2.set_data(-pos, edge_color=None, face_color=(0.5, 1, 1, 0.5), size=50)
        view.add(scatter)

    view.camera = 'turntable'  # or try 'arcball' 'turntable'
    # add a colored 3D axis for orientation
    axis = visuals.XYZAxis(parent=view.scene)
    vispy.app.run()
    
    ## Use render to generate an image object
    img=canvas.render()

    # Use write_png to export your wonderful plot as png ! 
    #io.write_png("clusters_"+str(cf.config['ediff_thr']*100)+"_percent.png",img)

    """
    if __name__ == '__main__':
        import sys
        if sys.flags.interactive != 1:
            vispy.app.run()
    """
    
    return 0


cpdef np.ndarray get_levels(Nanoparticle[:] nanops):

    cdef INT_t i = 0
    
    cdef np.ndarray e_levels, h_levels
      
    #print("Analyzing energy levels of sample %3d " %(sample))   # Suppressed 9_24
    
    # initialize the data struture for the quick union method
    e_levels = np.zeros(nanops.shape[0])
    h_levels = np.zeros(nanops.shape[0])
    
    # Start reading energy levels, convert back to eV
    for i in range(nanops.shape[0]):        
        e_levels[i] = nanops[i].cbenergy[0] / c.evtory
        h_levels[i] = nanops[i].vbenergy[0] / c.evtory
            
    return e_levels

"""
def gauss(x, p): # p[0]==mean, p[1]==stdev
    return 1.0/(p[1]*np.sqrt(2*np.pi))*np.exp(-(x-p[0])**2/(2*p[1]**2))



def fit_gaussian(results,no_bins,show_plot):

    xmax = -3.4
    xmin = -3.9
    
    #print('running from here')
    hist, bins = np.histogram(results, bins=no_bins, range=(xmin,xmax))
    width = 1 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    
    # Renormalize to a proper PDF
    hist = hist/( ((xmax-xmin)/no_bins)*hist.sum())
    
    # Fit a guassian
    p0 = [0,1] # Inital guess is a normal distribution
    
    errfunc = lambda p, x, y: gauss(x, p) - y # Distance to the target function
    
    p1, success = opt.leastsq(errfunc, p0[:], args=(center, hist))

    fit_mu, fit_stdev = p1

    FWHM = 2*np.sqrt(2*np.log(2))*fit_stdev

    #print(FWHM)
    
    if show_plot:
        plt.bar(center, hist, align='center', width=width)
        plt.plot(center, gauss(center,p1),lw=3, color='r')
        plt.xlabel('conduction band level [eV]',fontsize = 15)
        plt.ylabel('Nanoparticle count',fontsize = 15)
        plt.title('Energy distribution for conduction band',fontsize = 15)
        plt.xlim([xmin,xmax])
        plt.savefig('conduction_band_hist')#, dpi=200, figsize=(1600,1200))
        plt.show()

    return FWHM
"""

cdef np.ndarray gauss(np.ndarray x, np.ndarray p): # p[0]==mean, p[1]==stdev
    return 1.0/(p[1]*np.sqrt(2*np.pi))*np.exp(-(x-p[0])**2/(2*p[1]**2))


cdef FLOAT_t fit_gaussian(np.ndarray results, INT_t no_bins, bint show_plot):
    
    #cdef:
    #    FLOAT_t width, center, FWHM

    cdef FLOAT_t xmax = -3.4
    cdef FLOAT_t xmin = -3.9
    
    #print('running from here')
    hist, bins = np.histogram(results, bins=no_bins, range=(xmin,xmax))
    width = 1 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    
    # Renormalize to a proper PDF
    hist = hist / ((((xmax-xmin)/no_bins)*hist.sum()))   # hist /= ((xmax-xmin)/no_bins)*hist.sum()
    
    # Fit a guassian
    p0 = [0,1] # Inital guess is a normal distribution
    
    errfunc = lambda p, x, y: gauss(x, p) - y # Distance to the target function
    
    p1, success = opt.leastsq(errfunc, p0[:], args=(center, hist))

    fit_mu, fit_stdev = p1

    FWHM = 2*np.sqrt(2*np.log(2))*fit_stdev

    #print(FWHM)
    
    if show_plot:
        plt.bar(center, hist, align='center', width=width)
        plt.plot(center, gauss(center,p1),lw=3, color='r')
        plt.xlabel('conduction band level [eV]',fontsize = 15)
        plt.ylabel('Nanoparticle count',fontsize = 15)
        plt.title('Energy distribution for conduction band',fontsize = 15)
        plt.xlim([xmin,xmax])
        plt.savefig('conduction_band_hist')#, dpi=200, figsize=(1600,1200))
        plt.show()
    
    return FWHM




cpdef INT_t generate_random(INT_t sample_number, INT_t file_number, INT_t step_number, FLOAT_t thr):
    
    cdef INT_t r, rr
    cdef np.ndarray random
    cdef str ranfolder, ranfilename
    
    # Generate random numbers for simulation
    print("Generating random numbers for sample "+str(sample_number))                
    for r in range(file_number):
        
        random = np.zeros(step_number*2.5+10000)
        #print(random.size)

        for rr in range(random.size):
            random[rr] = np.random.random()
        
        ranfolder = 'input/random/'+str(thr)
        
        if not os.path.exists(ranfolder):
            os.makedirs(ranfolder)
        
        ranfilename = 'input/random/'+str(thr)+'/random'+str(r)+'.data'
        
        np.savetxt(ranfilename, random)
    print("finished generating random numbers")
    return 0



cpdef INT_t mobility_plot(str filename, list temperature_list, np.ndarray results):
    
    
    cdef INT_t sample_size = results.shape[0]
    cdef INT_t temp_steps = len(temperature_list)
    cdef INT_t sample_pairs = sample_size / 2
    
    cdef np.ndarray results_plot = np.zeros([sample_pairs,temp_steps])
    

    
    for temp in range(temp_steps):
        for sample in range(sample_pairs):
            results_plot[sample][temp] = (results[sample*2][temp]+results[sample*2+1][temp]) / 2
        
    #plt.ylim([0,1.1])
    plt.xlim([0, 450])
    #plt.yscale('log')
    plt.errorbar(temperature_list, results_plot.mean(axis=0), yerr=results_plot.std(axis=0)/np.sqrt(sample_size/2), color = 'blue')
    plt.xlabel('Temperature [K]',fontsize = 15)
    plt.ylabel('Mobility [cm^2 V /s]',fontsize = 15)
    plt.title('Mobility as a function of Temperature',fontsize = 15)
    
    plt.savefig('figures/'+filename+'.png')

    return 0
    



cpdef INT_t electron_info(Charge[:] electrons, np.ndarray record, INT_t step):
    
    cdef INT_t i
    #cdef np.ndarray        
    for i in range(electrons.shape[0]):            
        record[step, i] = electrons[i].particleindex
    
    return 0




cpdef INT_t visual_electrons(Charge[:] electrons, Nanoparticle[:] nanops):
    
    canvas = vispy.scene.SceneCanvas(keys='interactive', size=(600, 1200), show=True, bgcolor='white')
    view = canvas.central_widget.add_view()
    
    cdef INT_t i,j
    cdef np.ndarray pos, sz, pos_nanops, sz_nanops
    cdef INT_t nanoid    
     
    pos = np.zeros([electrons.shape[0],3])
    sz = np.zeros(electrons.shape[0])
    
    for i in range(electrons.shape[0]):            
        nanoid = electrons[i].particleindex            
        pos[i,0] = nanops[nanoid].x
        pos[i,1] = nanops[nanoid].y
        pos[i,2] = nanops[nanoid].z
        sz[i] = nanops[nanoid].radius
    
    scatter = visuals.Markers()
    scatter.set_data(pos, edge_color=None, face_color=(np.random.rand(), np.random.rand(), np.random.rand(), 1),size=sz)
    #scatter2.set_data(-pos, edge_color=None, face_color=(0.5, 1, 1, 0.5), size=50)
    view.add(scatter)

    pos_nanops = np.zeros([nanops.shape[0],3])
    sz_nanops = np.zeros(nanops.shape[0])


    for i in range(nanops.shape[0]):
        pos_nanops[i,0] = nanops[i].x
        pos_nanops[i,1] = nanops[i].y
        pos_nanops[i,2] = nanops[i].z
        sz_nanops[i] = nanops[i].radius
    
    scatter_nanops = visuals.Markers()
    scatter_nanops.set_data(pos_nanops, edge_color=None, face_color=(np.random.rand(), np.random.rand(), np.random.rand(), 0.2),size=sz_nanops)
    view.add(scatter_nanops)

    view.camera = 'turntable'  # or try 'arcball' 'turntable'
    # add a colored 3D axis for orientation
    axis = visuals.XYZAxis(parent=view.scene)
    vispy.app.run()
    
    ## Use render to generate an image object
    img=canvas.render()

    # Use write_png to export your wonderful plot as png ! 
    #io.write_png("clusters_"+str(cf.config['ediff_thr']*100)+"_percent.png",img)
    
    return 0


cdef FLOAT_t npnpdistance(Nanoparticle np1, Nanoparticle np2, Sample sample):
    cdef FLOAT_t distance2
    cdef FLOAT_t deltax, deltay, deltaz
    cdef FLOAT_t cellx, celly, cellz

    cellx = sample.cellx
    celly = sample.celly
    cellz = sample.cellz


    if sample.perx:
        deltax = fmin(fmin(fabs(np1.x-np2.x), fabs(np1.x-(np2.x-cellx))), fabs(np1.x-(np2.x+cellx)))
    else:
        deltax = np1.x - np2.x

    if sample.pery:
        deltay = fmin(fmin(fabs(np1.y-np2.y), fabs(np1.y-(np2.y-celly))), fabs(np1.y-(np2.y+celly)))
    else:
        deltay = np1.y - np2.y

    # force pbc in z direction

    deltaz = fmin(fmin(fabs(np1.z-np2.z), fabs(np1.z-(np2.z-cellz))), fabs(np1.z-(np2.z+cellz)))

    distance2 = deltax**2 + deltay**2 + deltaz**2
    #print(distance2)
    if (distance2 < 0):
        print('NP-NP overlap# New geometry needed#')
        return -1.0
    elif (distance2 == 0):
        print('NP-NP overlap# New geometry needed#')
        return -1.0
    else:
        return sqrt(distance2)-(np1.radius+np2.radius)


cpdef read_float(s):
    try:
        return float(s)
    except ValueError:
        if s == 'True':
            return True
        elif s == 'False':
            return False
        else:             
            return str(s)


cdef extern from "mtwist.c":
    uint32_t mt_seed()
    FLOAT_t mt_ldrand()


