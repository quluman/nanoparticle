
 
pi = 3.14159265358979323846
tpi = 2.0 * pi
fpi = 4.0 * pi
sqrtpi = 1.77245385090551602729 
sqrtpm1 = 1.0 / sqrtpi
sqrt2  = 1.41421356237309504880
 #
 # ... physical constants, si (nist codata 2006), web version 5.1
 #     http://physics.nist.gov/constants
h_planck_si      = 6.62606896e-34   # j s
k_boltzmann_si   = 1.3806504e-23    # j k^-1 
electron_si      = 1.602176487e-19  # c
electronvolt_si  = 1.602176487e-19  # j  
electronmass_si  = 9.10938215e-31   # kg
hartree_si       = 4.35974394e-18   # j
rydberg_si       = hartree_si/2.0   # j
bohr_radius_si   = 0.52917720859e-10 # m
amu_si           = 1.660538782e-27  # kg
c_si             = 2.99792458e+8    # m sec^-1
  # 
  # covert voltage in volt to ry atomic units 
  #
volt_ry          = electron_si/sqrt2/rydberg_si
ry_volt          = 1.0/volt_ry

  # ... physical constants, atomic units:
  # ... au for "hartree" atomic units (e = m = hbar = 1)
  # ... ry for "rydberg" atomic units (e^2=2, m=1/2, hbar=1)
  #
k_boltzmann_au   = k_boltzmann_si / hartree_si
k_boltzmann_ry   = k_boltzmann_si / rydberg_si
  #
  # ... unit conversion factors: energy and masses
  #
autoev           = hartree_si / electronvolt_si
rytoev           = autoev / 2.0
evtory           = 2.0 / autoev
amu_au           = amu_si / electronmass_si
amu_ry           = amu_au / 2.0
  #
  # ... unit conversion factors: atomic unit of time, in s and ps
  #
au_sec           = h_planck_si/tpi/hartree_si
au_ps            = au_sec * 1.0e+12
ry_sec           = h_planck_si/tpi/rydberg_si
ry_ps            = ry_sec * 1.0e+12
  #
  # ... unit conversion factors: pressure (1 pa = 1 j/m^3, 1gpa = 10 kbar )
  #
au_gpa           = hartree_si / bohr_radius_si ** 3 / 1.0e+9 
ry_kbar          = 10.0 * au_gpa / 2.0
  #
  # ... unit conversion factors: 1 debye = 10^-18 esu*cm 
  # ...                                  = 3.3356409519*10^-30 c*m 
  # ...                                  = 0.208194346 e*a
  # ... ( 1 esu = (0.1/c) am, c=299792458 m/s)
  #
debye_si         = 3.3356409519 * 1.0e-30 # c*m 
au_debye         = electron_si * bohr_radius_si / debye_si
  #
  # unit conversion for temperature 
  #
ev_to_kelvin = electronvolt_si / k_boltzmann_si
ry_to_kelvin = rydberg_si / k_boltzmann_si
kelvintory = k_boltzmann_si / rydberg_si
  # 
  # .. unit conversion factors: energy to wavelength
  #
evtonm = 1e+9 * h_planck_si * c_si / electronvolt_si
rytonm = 1e+9 * h_planck_si * c_si / rydberg_si
  #
  #  speed of light in atomic units
  #
c_au = c_si / bohr_radius_si * au_sec
  #
  # ... zero up to a given accuracy
  #
eps4  = 1.0e-4
eps6  = 1.0e-6
eps8  = 1.0e-8
eps12 = 1.0e-12
eps14 = 1.0e-14
eps16 = 1.0e-16
eps24 = 1.0e-24
eps32 = 1.0e-32
  #
e2 = 2.0      # the square of the electron charge
  #
  ###### compatibiility
  #
amconv = amu_ry
bohr_radius_cm = bohr_radius_si * 100.0
bohr_radius_angs = bohr_radius_cm * 1.0e8
nmtobohr = 10.0/bohr_radius_angs
bohrtonm = 1.0/nmtobohr
angstrom_au = 1.0/bohr_radius_angs
dip_debye = au_debye
au_terahertz  = au_ps
au_to_ohmcmm1 = 46000.0 # (ohm cm)^-1
ry_to_thz = 1.0 / au_terahertz / fpi
ry_to_cmm1 = 1.e+10 * ry_to_thz / c_si




