#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False


cimport constants as c
from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge
from event_class cimport Hopping, s_Hopping
from utility cimport npnpdistance
import numpy as np
cimport numpy as np
from libc.math cimport sqrt, fmin
from libc.math cimport fabs
from libc.math cimport exp
from libc.math cimport round

#nbnd = int(cf.config['nbnd'])
#voltage = float(cf.config['voltage'])
#cellz = float(cf.config['cell(3)'])

#sourcewf = float(cf.config['sourcewf'])
#drainwf = float(cf.config['drainwf'])
#lcapacitance0 = cf.config['lcapacitance0']
#lmarcus = cf.lmarcus
#lma = cf.lma

#jumpfreq = float(cf.config['jumpfreq'])
#emass = float(cf.config['emass'])
#temperature = float(cf.config['temperature'])
#marcusprefac2 = float(cf.config['marcusprefac2'])
#reorgenergy = float(cf.config['reorgenergy'])

#fitting = 7e-3



#######cpdef INT_t throw_electron(pc.Nanoparticle[:] nanops, pc.Charge[:] electrons, FLOAT_t[:] rand):
cdef INT_t throw_electron(Nanoparticle[:] nanops, s_Charge[:] electrons, Sample sample):
    
    cdef INT_t state, i, j, k, tries, elecindex, maxtry    
    #######cdef INT_t count = int(rand[0])
    cdef bint success
    
    maxtry = 1000
    # 1. determine where to throw electron randomly, they can land on the 
    # drain and source as well when there is pbc
    # 2. fill up states from below
    # 3. if the np is filled we draw a new random number
    for elecindex in range(sample.nelec):    
        success = False
        tries = 0
        while success == False:
            if (tries > maxtry):
                print('try exceeded maxtry!')
                break
            i =  int(np.random.random_integers(0, (nanops.shape[0]-1)))
            #######i = int(rand[count]*nanops.shape[0])
            #######count = count + 1
            j = 0
            # It is not very right here, only works for at most two energy levels. First level 0, second level 1 etc.
            if ( nanops[i].orboccupation[j] == nanops[i].orbmaxoccupation[j] ):
                #j = j + 1
                j += 1
            if ( j < sample.nbnd ):
                success = True    
            #tries = tries + 1
            tries += 1

        electrons[elecindex].particleindex = i
        electrons[elecindex].orbitalindex = j
        nanops[i].orboccupation[j] = nanops[i].orboccupation[j] + 1
        nanops[i].occupation = nanops[i].occupation + 1
        
        # Now testing memoview, it has no .sum() feature, but it is ok now since there is only 1 band
        #state = nanops[i].orbmaxoccupation.sum() - nanops[i].orbmaxoccupation[j] + nanops[i].orboccupation[j]
        state = 0        
        for k in range(sample.nbnd):
            state = state + nanops[i].orbmaxoccupation[k]            
        
        state = state - nanops[i].orbmaxoccupation[j] + nanops[i].orboccupation[j] - 1        
        nanops[i].electronindex[state] = elecindex
        electrons[elecindex].state = state
        #######rand[0] = float(count)

    
    return 0
    


cdef INT_t find_events_size(Nanoparticle[:] nanops, Charge[:] electrons, Sample sample):

    cdef INT_t i, j, l, sp, so, tp, to
  
    # Get the size of hopping events
    l = 0
    for i in range(electrons.shape[0]):
        sp = electrons[i].particleindex
        so = electrons[i].orbitalindex 
        #loop over neighbor particles
        for j in range(nanops[sp].nn):
            tp=nanops[sp].nnindex[j]
            # and look for free slots 
            for to in range(sample.nbnd):
                if (nanops[tp].orboccupation[to] < nanops[tp].orbmaxoccupation[to]):                    
                    l += 1

    return l


cdef INT_t find_events(Nanoparticle[:] nanops, Charge[:] electrons, Hopping[:] hoppings, Sample sample):

    cdef:
        INT_t i, j, l, sp, so, tp, to
        FLOAT_t rate
        FLOAT_t charging0energy  
        FLOAT_t chargingenergy
       
    # Setting up the events. Note set l=-1 here to ensure counting from 0 in the body
    l = -1
    # loop over electrons  
    for i in range(electrons.shape[0]):
        sp = electrons[i].particleindex
        so = electrons[i].orbitalindex 
        #loop over neighbor particles
        for j in range(nanops[sp].nn):
            tp=nanops[sp].nnindex[j]
            # and look for free slots 
            for to in range(sample.nbnd):
                if (nanops[tp].orboccupation[to] < nanops[tp].orbmaxoccupation[to]):                    
                    l += 1
                    hoppings[l].sourceparticle = sp
                    hoppings[l].sourceorbital = so
                    hoppings[l].targetparticle = tp
                    hoppings[l].targetorbital = to
                    hoppings[l].electronindex = i
                    # add kinetic energy difference
                    hoppings[l].energydiff = nanops[tp].cbenergy[to]-nanops[sp].cbenergy[so]                    
                    # add on-site (self) energy difference
                    if (sample.lcapacitance0):       
                        #print('executed')
                        # this function adds up the contribution due to the load of the first charge
                        charging0energy = nanops[tp].selfenergy0 - nanops[sp].selfenergy0
                        hoppings[l].energydiff = hoppings[l].energydiff + charging0energy
                    # this function adds up contribution from the self-energy due to the load of an additional (at least the second) charge
                    chargingenergy = nanops[tp].occupation*nanops[tp].selfenergy-(nanops[sp].occupation-1)*nanops[sp].selfenergy
                    hoppings[l].energydiff = hoppings[l].energydiff + chargingenergy
                    # add electron-hole interaction
                    hoppings[l].electronhole = electronholeeh(nanops, hoppings[l])
                    hoppings[l].energydiff = hoppings[l].energydiff + hoppings[l].electronhole
                    # add external potential 
                    hoppings[l].energydiff = hoppings[l].energydiff + electrons[i].charge*sample.voltage/sample.cellz*(nanops[tp].z-nanops[sp].z-sample.cellz*round((nanops[tp].z-nanops[sp].z)/sample.cellz)) 

                    # the above trick is not super safe if cellz is too small, just use something like
                    # deltaz=min(fabs(np1.z-np2.z),fabs(np1.z-(np2.z-cellz)), fabs(np1.z-(np2.z+cellz)))

                    # add built-in field
                    # hoppings[l].energydiff = hoppings[l].energydiff + electrons[i].charge/sqrt(2)*(sourcewf-drainwf)/cellz*(nanops[tp].z-nanops[sp].z)
                    # add electrotatic energy difference due to other dots        

                    ##!! No Possion yet !!
                    ##  hoppings[l].poisson=poisson(hopping[l])
                    ##  hoppings[l].energydiff = hoppings[l].energydiff + hoppings[l].poisson
                    
                    # calculate rate and multiply it with the final state degeneracy initial state degeneracy is taken into account by summing over all electrons
                    # room for optimization: only sum over particles and explicitly take into account initial state degeneracy, this becomes important if there is more
                    # than one electron per nanoparticle
                    hoppings[l].rate=(nanops[tp].orbmaxoccupation[to]-nanops[tp].orboccupation[to])*get_rate(nanops, hoppings[l], sample)
    

    # (l+1) is number of electron hopping events
    return (l+1)



cdef INT_t execute_event(Nanoparticle[:] nanops, s_Charge[:] elec, s_Hopping executedhopping): # no drain or source is used
  
    cdef INT_t i, j, state
    cdef INT_t tp, to, sp, so
    cdef INT_t current
    
    tp = executedhopping.targetparticle
    to = executedhopping.targetorbital
    sp = executedhopping.sourceparticle
    so = executedhopping.sourceorbital
    
    nanops[tp].hotness = nanops[tp].hotness + 1 
   
    current = 0
    
    state = elec[executedhopping.electronindex].state

    nanops[sp].electronindex[state] = -1

    #state = findelecnewstate(executedhopping.targetparticle,executedhopping.targetorbital)
    #state = nanops[tp].orbmaxoccupation.sum() - nanops[tp].orbmaxoccupation[to] + nanops[tp].orboccupation[to]    
    state = findelecnewstate(nanops, tp, to)
        
    nanops[tp].electronindex[state] = executedhopping.electronindex

    nanops[sp].orboccupation[so] = nanops[sp].orboccupation[so] - 1
    
    nanops[tp].orboccupation[to] = nanops[tp].orboccupation[to] + 1
    
    nanops[sp].occupation = nanops[sp].occupation - 1
    
    nanops[tp].occupation = nanops[tp].occupation + 1
   
    # update hopping electron

    elec[executedhopping.electronindex].particleindex = tp
    elec[executedhopping.electronindex].orbitalindex = to
    elec[executedhopping.electronindex].state = state
    
    if (nanops[tp].drain and nanops[sp].source):

        current = -1
    
    if (nanops[sp].drain and nanops[tp].source):

        current = 1

    return current

### Funtions used in this part
 
cdef FLOAT_t electronholeeh(Nanoparticle[:] nanops, s_Hopping hopping):

    cdef INT_t sp, tp
    cdef FLOAT_t temp, exconsp, excontp

    sp = hopping.sourceparticle
    tp = hopping.targetparticle
    exconsp = fmin(nanops[sp].vboccupation,nanops[sp].occupation)
    excontp = fmin(nanops[tp].vboccupation,nanops[tp].occupation+1)
    # first let's take care of the self energy cancellation
    # due to the first electron-hole pair
    temp = 0
    if (excontp > 0):
        temp = -nanops[tp].selfenergy0
    if (exconsp > 0): 
      # minus*minus=plus
        temp = temp + nanops[sp].selfenergy0
    # then take care of the self energy cancellation of additional electron-hole pairs, the multiple
    # exciton binding energy is just the sum of single exciton binding energies
    exconsp=fmin(nanops[sp].vboccupation,nanops[sp].occupation-1)
    excontp=fmin(nanops[tp].vboccupation,nanops[tp].occupation)

    temp = temp - excontp*nanops[tp].selfenergy + exconsp*nanops[sp].selfenergy

    return temp
  



cdef FLOAT_t get_rate(Nanoparticle[:] nanops, s_Hopping hopping, Sample sample):
    
    cdef FLOAT_t overlap, rrate, rate
    
    cdef INT_t tp, to, sp, so
    
    tp = hopping.targetparticle
    to = hopping.targetorbital
    sp = hopping.sourceparticle
    so = hopping.sourceorbital    
    
    rate = 0
    rrate = 0
    # in Ry atomic units m=1/2, e^2=2, hbar=1
    # used here Chandler's approximation with the average energies
    # is this mass the electron/hole effective mass? than it should be different for holes and electrons
    overlap = sqrt(-sample.emass*(nanops[sp].cbenergy[so] + nanops[tp].cbenergy[to]) / 2.0)

    if (sample.lmarcus):
        rate = c.tpi*sample.marcusprefac2*exp(-2.0 * npnpdistance(nanops[sp], nanops[tp], sample) * overlap) / sqrt(c.fpi*sample.reorgenergy*sample.temperature)
        rrate = rate*exp(-(sample.reorgenergy+hopping.energydiff)**2.0/(4.0*sample.reorgenergy*sample.temperature))

    elif (sample.lma):
        rate = sample.jumpfreq*exp(-2.0 *npnpdistance(nanops[sp], nanops[tp], sample)*overlap)

        if hopping.energydiff > 0.0 :
            rrate = rate*exp(-hopping.energydiff/sample.temperature)
        else: 
            rrate = rate
    return rrate

        
    
cdef INT_t findelecnewstate(Nanoparticle[:] nanops, INT_t p, INT_t o):
    # p is nanoparticle number, o is orbital number
    cdef INT_t states
    cdef INT_t i
    
    states = 0
    
    for i in range(nanops[p].orbmaxoccupation.shape[0]):
        states +=  nanops[p].orbmaxoccupation[i]
    
    states = states - nanops[p].orbmaxoccupation[o] + nanops[p].orboccupation[o]    
    return states


### Working on this part. Include intra-cluster hopping mechanism.

cdef INT_t find_events_cluster(Nanoparticle[:] nanops, s_Charge[:] electrons, s_Hopping[:] hoppings, Sample sample):    #, pc.Cluster[:] clusterz):
    cdef:
        INT_t i, j, l, sp, so, tp, to, cluster_num
        FLOAT_t rate
        FLOAT_t charging0energy  
        FLOAT_t chargingenergy
        FLOAT_t cellz
    
    cellz = sample.cellz
    # Setting up the events. Note set l=-1 here to ensure counting from 0 in the body
    l = -1
    # loop over electrons  
    for i in range(sample.nelec):
        sp = electrons[i].particleindex
        so = electrons[i].orbitalindex         
        
        ###########################
        # Simplified case version #
        ###########################
       
        for j in range(nanops[sp].nn):
            tp=nanops[sp].nnindex[j]
            # and look for free slots executedhopping
            for to in range(sample.nbnd):
                if (nanops[tp].orboccupation[to] < nanops[tp].orbmaxoccupation[to]):                    
                    
                    
                    # There should be TWO cases here. Regular hopping or hopping onto a close neighbor
                    
                    if nanops[sp].if_cn[j] == 0:  # This is the regular hopping version
                        
                        l += 1
                        hoppings[l].sourceparticle = sp
                        hoppings[l].sourceorbital = so
                        hoppings[l].targetparticle = tp
                        hoppings[l].targetorbital = to
                        hoppings[l].electronindex = i
                    
                        # add kinetic energy difference
                        hoppings[l].energydiff = nanops[tp].cbenergy[to]-nanops[sp].cbenergy[so]                    
                        # add on-site (self) energy difference
                        if (sample.lcapacitance0):       
                            # this function adds up the contribution due to the load of the first charge
                            charging0energy = nanops[tp].selfenergy0 - nanops[sp].selfenergy0
                            hoppings[l].energydiff = hoppings[l].energydiff + charging0energy
                        # this function adds up contribution from the self-energy due to the load of an additional (at least the second) charge
                        chargingenergy = nanops[tp].occupation*nanops[tp].selfenergy-(nanops[sp].occupation-1)*nanops[sp].selfenergy
                        hoppings[l].energydiff = hoppings[l].energydiff + chargingenergy
                        # add electron-hole interaction
                        hoppings[l].electronhole = electronholeeh(nanops, hoppings[l])
                        hoppings[l].energydiff = hoppings[l].energydiff + hoppings[l].electronhole
                        # add external potential 
                        hoppings[l].energydiff = hoppings[l].energydiff + electrons[i].charge*sample.voltage/cellz*(nanops[tp].z-nanops[sp].z-cellz*round((nanops[tp].z-nanops[sp].z)/cellz))

                        # add built-in field
                        # hoppings[l].energydiff = hoppings[l].energydiff + electrons[i].charge/sqrt(2)*(sourcewf-drainwf)/cellz*(nanops[tp].z-nanops[sp].z)
                        # add electrotatic energy difference due to other dots: Poisson part, not ready yet
                        # calculate rate and multiply it with the final state degeneracy initial state degeneracy is taken into account by summing over all electrons
                        hoppings[l].rate=(nanops[tp].orbmaxoccupation[to]-nanops[tp].orboccupation[to])*get_rate(nanops, hoppings[l], sample)
                        hoppings[l].intra = 0
   
                        
                    if nanops[sp].if_cn[j] == 1: # This is the intra-cluster hopping version
                        
                        # Allow hoppings towards drain only
                        if nanops[tp].z-nanops[sp].z-cellz*round((nanops[tp].z-nanops[sp].z)/cellz)  > 0:
                            
                            l += 1
                            hoppings[l].sourceparticle = sp
                            hoppings[l].sourceorbital = so
                            hoppings[l].targetparticle = tp
                            hoppings[l].targetorbital = to
                            hoppings[l].electronindex = i
                            
                            hoppings[l].rate=(nanops[tp].orbmaxoccupation[to]-nanops[tp].orboccupation[to])*(sample.fitting)*sample.voltage/cellz*(nanops[tp].z-nanops[sp].z-cellz*round((nanops[tp].z-nanops[sp].z)/cellz))       
                            hoppings[l].intra = 1
                       

                           # Here we need to think it really carefully. The rate should be inversely proportional to the travel distance, and multiply with a fitting parameter
                            # Refer to Matt Law figure?
  

    # (l+1) is number of electron hopping events
    return (l+1)






