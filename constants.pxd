cimport numpy as cnp

ctypedef cnp.float64_t FLOAT_t
ctypedef cnp.intp_t INT_t
ctypedef cnp.ulong_t INDEX_t
ctypedef cnp.uint8_t BOOL_t
 
cdef  FLOAT_t pi
cdef  FLOAT_t tpi 
cdef  FLOAT_t fpi 
cdef  FLOAT_t sqrtpi 
cdef  FLOAT_t sqrtpm1 
cdef  FLOAT_t sqrt2  
 #
 # ... physical constants, si (nist codata 2006), web version 5.1
 #     http://physics.nist.gov/constants
cdef  FLOAT_t h_planck_si      
cdef  FLOAT_t k_boltzmann_si  
cdef  FLOAT_t electron_si      
cdef  FLOAT_t electronvolt_si 
cdef  FLOAT_t electronmass_si 
cdef  FLOAT_t hartree_si      
cdef  FLOAT_t rydberg_si      
cdef  FLOAT_t bohr_radius_si  
cdef  FLOAT_t amu_si          
cdef  FLOAT_t c_si            
  # 
  # covert voltage in volt to ry atomic units 
  #
cdef  FLOAT_t volt_ry         
cdef  FLOAT_t ry_volt        

  # ... physical constants, atomic units:
  # ... au for "hartree" atomic units (e = m = hbar = 1)
  # ... ry for "rydberg" atomic units (e^2=2, m=1/2, hbar=1)
  #
cdef  FLOAT_t k_boltzmann_au  
cdef  FLOAT_t k_boltzmann_ry   
  #
  # ... unit conversion factors: energy and masses
  #
cdef  FLOAT_t autoev          
cdef  FLOAT_t rytoev           
cdef  FLOAT_t evtory         
cdef  FLOAT_t amu_au           
cdef  FLOAT_t amu_ry          
  #
  # ... unit conversion factors: atomic unit of time, in s and ps
  #
cdef  FLOAT_t au_sec           
cdef  FLOAT_t au_ps           
cdef  FLOAT_t ry_sec       
cdef  FLOAT_t ry_ps         
  #
  # ... unit conversion factors: pressure (1 pa = 1 j/m^3, 1gpa = 10 kbar )
  #
cdef  FLOAT_t au_gpa          
cdef  FLOAT_t ry_kbar          

  # ... unit conversion factors: 1 debye = 10^-18 esu*cm 
  # ...                                  = 3.3356409519*10^-30 c*m 
  # ...                                  = 0.208194346 e*a
  # ... ( 1 esu = (0.1/c) am, c=299792458 m/s)
  #
cdef  FLOAT_t debye_si         
cdef  FLOAT_t au_debye         
  #
  # unit conversion for temperature 
  #
cdef  FLOAT_t ev_to_kelvin 
cdef  FLOAT_t ry_to_kelvin
cdef  FLOAT_t kelvintory 
  # 
  # .. unit conversion factors: energy to wavelength
  #
cdef  FLOAT_t evtonm 
cdef  FLOAT_t rytonm 
  #
  #  speed of light in atomic units
  #
cdef  FLOAT_t c_au 

  # ... zero up to a given accuracy
  #
cdef  FLOAT_t eps4 
cdef  FLOAT_t eps6  
cdef  FLOAT_t eps8  
cdef  FLOAT_t eps12 
cdef  FLOAT_t eps14 
cdef  FLOAT_t eps16 
cdef  FLOAT_t eps24 
cdef  FLOAT_t eps32 
  #
cdef  FLOAT_t e2 # the square of the electron charge
  #
  ###### compatibiility
  #
cdef  FLOAT_t amconv 
cdef  FLOAT_t bohr_radius_cm 
cdef  FLOAT_t bohr_radius_angs 
cdef  FLOAT_t nmtobohr 
cdef  FLOAT_t bohrtonm 
cdef  FLOAT_t angstrom_au 
cdef  FLOAT_t dip_debye 
cdef  FLOAT_t au_terahertz  
cdef  FLOAT_t au_to_ohmcmm1 
cdef  FLOAT_t ry_to_thz 
cdef  FLOAT_t ry_to_cmm1 




