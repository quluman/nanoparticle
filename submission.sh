#!/bin/bash
#$ -cwd
#$ -j no
#$ -N e50_0.32_0.36_1
#$ -S /bin/bash
#$ -e n_400_e_50_0.32_0.36_1.err
#$ -o n_400_e_50_0.32_0.36_1.out
#$ -l qname=all.q

source /share/apps/environment.sh

./main.py 50 400 0.32 3
