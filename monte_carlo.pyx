#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False
from particle_class cimport Sample, Nanoparticle, Electrode, Charge, s_Charge

from event_class cimport Hopping, s_Hopping

from electron_library cimport throw_electron, find_events_cluster, execute_event

from hole_library cimport throw_hole, find_hole_events_cluster, execute_hole_event

cimport routines 

from routines cimport linearsearch, numpysearch, numpysearch_fast

cimport utility as util
cimport constants as c

import numpy as np
cimport numpy as np
import time

from libc.math cimport log, fabs




cpdef monte_carlo(INT_t steps, INT_t sample_no, INT_t e_number, INT_t h_number, INT_t nanops_number, FLOAT_t temp, FLOAT_t thr, str features, output):
    
    
    util.mt_seed()
    
    
    cdef:
        INT_t i,j
        INT_t totalsteps = steps
        FLOAT_t eleccurrent = 0
        FLOAT_t holecurrent = 0
        FLOAT_t totalrate = 0
        FLOAT_t time_ry = 0
        FLOAT_t time_ps = 0
        FLOAT_t mobility = 0
        FLOAT_t icurrent = 0
        FLOAT_t[:,:] data_view = np.zeros([totalsteps,5])       

        FLOAT_t ave_current = 0 
        FLOAT_t ave_mobility = 0
 
        FLOAT_t t_start,t_end
        INT_t n_events, n_hevents
    
    
    t_start = float(time.clock())
    
    # Initialize a sample
    cdef Sample sample = Sample()    
    sample.set_environment()
    
    sample.nelec = e_number
    sample.nhole = h_number
    sample.nnanops = nanops_number
    sample.sample_number = sample_no
    sample.temperature = temp * c.kelvintory

    
    
    cdef: 
        
        
        s_Charge s_electrons_def[200]
        s_Charge s_holes_def[200]
        
        
        s_Hopping elec_hopping_def[3000]
        s_Hopping hole_hopping_def[3000]
        s_Hopping exe_event
        
        Electrode source = Electrode()
        Electrode drain = Electrode()
        
    
 
 
    # Initialize nanoparticles
    cdef Nanoparticle[:] nanoparticles = routines.set_nanoparticles(sample)
    
    #print('FWHM', sample.FWHM)
    
    sample.ediff_thr = sample.FWHM * c.evtory * thr
    
    # Initialize charges
    cdef s_Charge[:] s_electrons = s_electrons_def 
    cdef s_Charge[:] s_holes = s_holes_def
            
    routines.set_s_electrons(s_electrons, sample)
    routines.set_s_holes(s_holes, sample)
    
    #electrons = routines.set_electrons(sample)
    #holes = routines.set_holes(sample)
    
    
  
    # Initialize sample properties
    routines.dielectrics(nanoparticles, sample)
    
    routines.neighborlist(nanoparticles, sample)
    
    routines.find_electrode(nanoparticles, source, drain, sample)
    
    # Get some constants
    cdef: 
        FLOAT_t cellz = sample.cellz
        FLOAT_t cellz_cm = sample.cellz_nm * 1e-7
        FLOAT_t voltage = sample.voltage
        INT_t nelec = sample.nelec
        FLOAT_t bohrtonm = c.bohrtonm
        FLOAT_t ry_ps = c.ry_ps 
        FLOAT_t volt_ry = c.volt_ry

    
    # Initialize events
    cdef s_Hopping[:] elec_hopping = elec_hopping_def
    
    cdef s_Hopping[:] hole_hopping = hole_hopping_def
    
    for i in range(3000):
        elec_hopping[i].hopping_type = 0
        hole_hopping[i].hopping_type = 1
    
    
    #cdef np.ndarray[FLOAT_t] probability = np.zeros((elec_hopping.shape[0]+hole_hopping.shape[0]), dtype = np.float)
        
    # throw charges    
    throw_electron(nanoparticles, s_electrons, sample)
    
    throw_hole(nanoparticles, s_holes, sample)     
    
    if (temp == 50):
        print('fitting is ', sample.fitting )


    """
    if ((sample_no ==0) and (temp == 50)):
        print(cellz, voltage/volt_ry)
        print(sample.marcusprefac2)
        print(sample.jumpfreq)
        print(sample.emass)
        print(sample.hmass) 
        print(sample.reorgenergy)
        print(sample.ligandlength)
        print(sample.sourcewf)
        print(sample.drainwf)
        print(sample.bradii)
        print(sample.capacitance)
        print('temperature',sample.temperature)       # read from script
        print('voltage',sample.voltage)           # read from script
        print(sample.ndist_thr)
        print(sample.ediff_thr)
        print(sample.dcout)
        print(sample.e_degeneracy)
        print(sample.h_degeneracy)
        print(sample.cellx)
        print(sample.celly)
        print(sample.cellz)
        print(sample.cellz_nm)
        print(sample.beta)
        print(sample.nbnd)
        print(sample.packingfraction)
        print(sample.nelec)    # number of electrons
        print(sample.nhole)    # number of holes
        print(sample.nnanops)   # number of nanoparticles
        print(sample.sample_number)  # store the nanoparticle file to open
        print(sample.npdc)
        print(sample.liganddc)
        print(sample.FWHM)
        print(sample.fitting)
        print(sample.lma)
    """   
        
        

    # start of monte-carlo simulation
    for i in range(totalsteps):
        
     
        # Find electron events
        n_events = find_events_cluster(nanoparticles, s_electrons, elec_hopping, sample) 
        
        # Find hole events
        n_hevents = find_hole_events_cluster(nanoparticles, s_holes, hole_hopping, sample)
       
        
        # Select an event to excute
        exe_event = linearsearch(elec_hopping, hole_hopping, n_events, n_hevents)
        #exe_event = numpysearch(elec_hopping, hole_hopping, n_events, n_hevents, probability)
        #exe_event = numpysearch_fast(elec_hopping, hole_hopping, n_events, n_hevents, rate_array)
 
        totalrate = exe_event.totalrate     
        
        
        """
        #print(exe_event.hopping_type)
        #print(exe_event.electronindex)
        #print(exe_event.sourceparticle)
        #print(exe_event.targetparticle)
        if exe_event.intra == 0:
            print('0')
        #print(exe_event.rate)
        #print(exe_event.totalrate)      
   
        
        if ( (i % 10000 == 0) and (sample_no ==0) and (temp == 400)):
            print(time_ps, eleccurrent)
        """
        
        if (exe_event.hopping_type == 1):
          
            holecurrent = holecurrent + execute_hole_event(nanoparticles, s_holes, exe_event)
            

        else:    #(exe_event.hopping_type == 0):
    
            eleccurrent = eleccurrent + execute_event(nanoparticles, s_electrons, exe_event)

        #time_ry = time_ry - log(1-np.random.random()) / totalrate 
        time_ry = time_ry - log(1-util.mt_ldrand()) / totalrate 
 
        time_ps = time_ry * ry_ps
  
        # Get mobility in cm^2 V /s
        mobility = (eleccurrent*cellz*bohrtonm*cellz*bohrtonm)*0.01*volt_ry/(time_ps*voltage*nelec)
        
        # Get current in A
        icurrent = eleccurrent*(1.6e-19) / (time_ps*(1.0e-12))
       
                
        data_view[i,0] = time_ps          # time_ry elec_hoppings.shape[0]
        data_view[i,1] = totalrate
        data_view[i,2] = mobility         #  log(fabs(mobility))     
        data_view[i,3] = eleccurrent
        data_view[i,4] = icurrent
        # end of simulation loop
    
    
    # Calculate ave_current, in order to get I-V curve
    if features == 'iv':
        for j in range(5000):
            ave_current += data_view[i-j,4]
        ### so the unit of output is really A/cm2, the current density
        output.put([sample_no, temp, ave_current*cellz_cm*cellz_cm/5000])   
        
    # Calculate ave_mobility, in order to get mobility-T curve
    if features == 'mobility':
        for j in range(5000):
            ave_mobility += data_view[i-j,2]
        output.put([sample_no, temp, ave_mobility/5000])
    
    
    t_end = float(time.clock())        
    
    print("sample %2d at temperature %3d finishes with time %6.2f s" % (sample_no, temp, (t_end-t_start) ))
    
    return 0
 
