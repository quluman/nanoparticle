import numpy as np
cimport numpy as np

cdef class Hopping:
    def __cinit__(Hopping self, int info):  
        self.electronindex = 0
        self.holeindex = 0 
        self.chargeindex = 0    
        self.sourceparticle = 0           
        self.sourceorbital = 0          
        self.targetparticle = 0   
        self.targetorbital = 0   
        self.energydiff = 0   
        self.rate = 0   
        self.poisson = 0   
        self.electronhole = 0 
        self.hopping_type = info    # type = 0 for electron hopping, 1 for hole hopping
        self.totalrate = 0
        self.intra = 0  #  =0 for normal, =1 for intra 
        
        

    

    