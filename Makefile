all:
	cp input/rng/*.* .
	python3 setup.py build_ext --inplace 

clean:
	rm -r build *.c *.so __pycache__ *.pyx~ *.py~ *.h Makefile~ *.html
