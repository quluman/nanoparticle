
#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False


cimport constants as c

import numpy as np
cimport numpy as np
cimport utility as util


cdef class Nanoparticle:
    def __cinit__(Nanoparticle self):
       
        self.nn = 0                                     # number of nearest neighbors
        self.nnindex = np.empty(2,dtype = np.int)       # nearest neighbor index
        self.nndistance = np.empty(2,dtype = np.float)
        
        self.cn = 0                                     # number of close neighbors (electron)                                
        self.cnindex = np.empty(2,dtype = np.int)     # close neighbor index (electron)
        self.if_cn = np.empty(2,dtype = np.int)       # 0 for not being close neighbor, 1 for being close neighbor
        
        
        self.hcn = 0                                     # number of close neighbors (hole)                                 
        self.hcnindex = np.empty(2,dtype = np.int)     # close neighbor index  (hole)  
        self.if_hcn = np.empty(2,dtype = np.int)     # 0 for not being close neighbor, 1 for being close neighbor
        
        
        self.cluster = -1    # in which cluster.if cluster = -1, it means it is not in a cluster yet
        self.incluster = False
        self.edge = False        
        
        
        self.diameter = 0              #
        self.radius = 0              #
        self.ligandlength = 0        #
        self.x = 0 
        self.y = 0
        self.z = 0# 
        self.hotness = 0 # to measure how often this nanoparticle is visited
        self.cbenergy = np.zeros(2,dtype = np.double)
        self.cbenergy1 = 0
        self.cbenergy2 = 0       #
        self.vbenergy = np.zeros(2,dtype = np.double)        
        self.vbenergy1 = 0           #
        self.vbenergy2 = 0
        self.selfenergy0 = 0         #
        self.selfenergy = 0          # 
        self.dcin = 0 #cf.config['npdc']   #
        self.hselfenergy0 = 0            #
        self.hselfenergy = 0             #
        self.source = False # source 
        self.drain = False # drain
        

        self.electronindex = np.zeros(2,dtype = np.int)        ## To be removed
        self.orboccupation = np.zeros(2,dtype = np.int)
        self.orbmaxoccupation = np.zeros(2,dtype = np.int)
        self.occupation = 0 # needed for on site energies

        self.holeindex = np.zeros(2,dtype = np.int)           ## To be removed
        self.vborboccupation = np.zeros(2,dtype = np.int)
        self.vborbmaxoccupation = np.zeros(2,dtype = np.int)
        self.vboccupation = 0 # needed for on site energies
    
    cpdef set_cbenergy1(Nanoparticle self):
    # Kand and Wise, d in nm, energy in eV
    # cbenergy=evtory*(0.238383 +4.28063 *diameter**(-1.87117 ))
    # localdiam=diameter*bohrtonm
        self.cbenergy1 = c.evtory*(-4.238383 + 4.28063*(self.diameter*c.bohrtonm)**(-1.87117))
        self.cbenergy[0] = c.evtory*(-4.238383 + 4.28063*(self.diameter*c.bohrtonm)**(-1.87117))
   
        
    cpdef set_cbenergy2(Nanoparticle self):
    # Kand and Wise, d in nm, energy in eV
    # localdiam=diameter*bohrtonm
    # cbenergy2=0.254004 +8.0018 *diameter**(-1.82088 )
        self.cbenergy2 = c.evtory*(-4.254004 + 8.0018*(self.diameter*c.bohrtonm)**(-1.82088))
        self.cbenergy[1] = c.evtory*(-4.254004 + 8.0018*(self.diameter*c.bohrtonm)**(-1.82088))
        
    
    cpdef set_vbenergy(Nanoparticle self):
    # Kand and Wise, d in nm, energy in eV
    # localdiam=diameter*bohrtonm
    # vbenergy=evtory*(-0.220257 -5.23931 *diameter**(-1.85753 ))
        self.vbenergy1 = c.evtory*(-4.728265 - 5.23931*(self.diameter*c.bohrtonm)**(-1.85753))
        self.vbenergy[0] = c.evtory*(-4.728265 - 5.23931*(self.diameter*c.bohrtonm)**(-1.85753))

    cpdef set_selfenergy0(Nanoparticle self, Sample sample): 
    # this function adds up the contribution
    # due to the load of the first charge
        #print('dcout', cf.config['dcout'])
        if (sample.lcapzunger):
            #print('should not excuted')
            self.selfenergy0=c.e2/(2.0 *sample.capacitance*self.radius)
        elif (sample.lcapdelerue): 
            #print('executed')  
            #print('dcin', self.dcin)            
            self.selfenergy0 = (c.e2/self.radius)*((0.5/sample.dcout - 0.5/self.dcin) + 0.47*(self.dcin-sample.dcout)/((self.dcin+sample.dcout)*self.dcin)) 
            
            #### Test here            
            
    cpdef set_selfenergy(Nanoparticle self, Sample sample):
    # this function adds up contribution
    # from the self-energy due to the 
    # load of an additional (at least the second) charge    
        if (sample.lcapzunger):        
            self.selfenergy=c.e2/(2.0 *sample.capacitance*self.radius)
        elif (sample.lcapdelerue):
            self.selfenergy=c.e2/self.radius*(1.0 / sample.dcout + 0.79 /self.dcin)
         
        
    cpdef set_holeselfenergy0(Nanoparticle self, Sample sample):
    # this function adds up the contribution
    # due to the load of the first charge   
        if (sample.lcapzunger):
            self.hselfenergy0=c.e2/(2.0 *sample.capacitance*self.radius)
        elif (sample.lcapdelerue):
            self.hselfenergy0=c.e2/self.radius*((0.5 /sample.dcout-0.5 /self.dcin)+0.47 /self.dcin*(self.dcin - sample.dcout)/(self.dcin + sample.dcout))
    
    cpdef set_holeselfenergy(Nanoparticle self, Sample sample):
    # this function adds up contribution from the self-energy due to the
    # load of an additional (at least the second) charge
        if (sample.lcapzunger):
            self.hselfenergy=c.e2/(2.0 *sample.capacitance*self.radius)
        elif (sample.lcapdelerue):
            self.hselfenergy=c.e2/self.radius*(1.0 / sample.dcout + 0.79 /self.dcin)
    

cdef class Electrode:
    def __cinit__(Electrode self):
        self.nnindex = np.empty(2,dtype = np.int)
        self.electronindex = 0    ### to be removed
        self.holeindex = 0        ### to be removed
        self.elecocc = 0
        self.holeocc = 0
        self.nn = 0
        self.energy = 0
        self.wf = 0
         # integer :: occ 
         # there can only be one electron waiting
        

cdef class Charge:
    def __cinit__(Charge self):
        self.particleindex = 0   ### To be removed
        self.orbitalindex = 0   ### To be removed
        self.charge = 0
        self.mass = 0
        self.energy = 0
        self.on_electrode = False 
        self.state = 0
    

## 8_28 stopped here.
## need to finished coding the sorted version of npindex and z distance. This will help find the target particle for the intra-cluster hopping


cdef class Cluster:
    def __cinit__(Cluster self):
        self.size = 0                                            # no. of nanoparticles in the cluster
        self.npindex = np.empty(2,dtype = np.int)                # indexes of the nanoparticles in the cluster
        self.zdistance = np.empty(2,dtype = np.float)               # distance in z direction for each member
        #self.ordered_z = np.empty(2,dtype = np.int)               # ordered list of z
        self.ordered_index = np.empty(200,dtype = np.int)              # ordered list of np id
        self.reverse_index = np.empty(200,dtype = np.int)              # reverse of the list, used for hole hoppings
        self.has_source = False
        self.has_drain = False
        self.percolates = False
    
    cpdef set_size(self):
        
        self.zdistance = np.empty(self.size,dtype = np.float)               # distance in z direction for each member
        self.ordered_index = np.empty(self.size,dtype = np.int)              # ordered list of np id
        self.reverse_index = np.empty(self.size,dtype = np.int)
        
    
    cpdef find_target(self):
        
        # z=0 plane is where electrons flow into the system
        # z=L plane is where electrons are collected
    
        self.ordered_index = self.npindex[self.zdistance.argsort()]        #  ordered_index[0] is closest to source, ordered_index[-1] is closest to drain
        self.reverse_index = self.npindex[self.zdistance.argsort()][::-1]  
        
    cpdef find_percolation(self):
        self.percolates = (self.has_drain) and (self.has_source)



cdef class Sample:
    def __cinit__(Cluster self):
        
        # System logicals
        lbinarytree = False
        lheap = False
        llinearsearch = False
        lquick = False
        lcapzunger = False
        lcapdelerue = False
        lempnone = False
        lemmg = False
        lemla = False
        lemlll = False
        lma = False
        lmarcus = False
        nanoparticlesim = False
        nanorodsim = False
        lpoissonnone = False
        lpoissonewald = False
        lpoissonnn = False
        pennmodel = False
        lcapacitance0 = False
        perx = False
        pery = False
        perz = False
        
        # System physical values, read from file or script        
        marcusprefac2 = 0
        jumpfreq = 0
        emass = 0
        hmass = 0 
        reorgenergy = 0
        ligandlength = 0
        sourcewf = 0
        drainwf = 0
        bradii = 0
        capacitance = 0
        temperature = 0       # read from script
        voltage = 0           # read from script
        ndist_thr = 0
        ediff_thr = 0
        dcout = 0
        e_degeneracy = 0
        h_degeneracy = 0
        cellx = 0
        celly = 0
        cellz = 0
        cellz_nm = 0
        beta = 0
        nbnd = 0
        packingfraction = 0
        nelec = 0    # number of electrons
        nhole = 0    # number of holes
        nnanops = 0   # number of nanoparticles
        sample_number = 0  # store the nanoparticle file to open
        npdc = 0
        liganddc = 0
        FWHM = 0
        fitting = 1


    cpdef set_environment(self):
        
        cdef: 
            dict config = {}
            str line
            list columns
        
        
        # Create a temp dictionary
        f = open('input/montecarlo.inp', 'r')
        f.readline()
        #montecarlo = dict()
        for line in f:
            line = line.strip()
            columns = line.split()
        #    montecarlo[columns[0]] = columns[2]
            config[columns[0]] = util.read_float(columns[2])
        f.close()    
        #   print(config)
        # Create system dictionary
        f = open('input/system.inp', 'r')
        f.readline()
        #system = dict()
        for line in f:
            line = line.strip()
            columns = line.split()
            #    system[columns[0]] = columns[2]
            config[columns[0]] = util.read_float(columns[2])
        f.close()   
        #print(system)
    
        # Create lattice dictionary
        f = open('input/lattice.inp', 'r')
        f.readline()
        #lattice = dict()
        for line in f:
            line = line.strip()
            columns = line.split()
        #    lattice[columns[0]] = columns[2]
            config[columns[0]] = util.read_float(columns[2])
        f.close()

        # Then give the configure the governing logical variables 

        if config['lcapacitance0'] == 'True':
            self.lcapacitance0 = True

        if config['sort_flavor'] == 'linearsearch':
            self.llinearsearch = True
        elif config['sort_flavor'] == 'quick':
            self.lquick = True
        elif config['sort_flavor'] == 'heap':
            self.lheap = True
        elif config['sort_flavor'] == 'binarytree':
            self.lbinarytree = True
        else: 
            print('error in the sort flavor')

        if config['capacitance_model'] == 'delerue':
            self.lcapdelerue = True
        elif config['capacitance_model'] == 'zunger':
            self.lcapzunger = True
        else:
            print('error in the capacitance model')
        
        if config['effective_medium'] == 'none':
            self.lempnone = True
        elif config['effective_medium'] == 'mg':
            self.lemmg = True
        elif config['effective_medium'] == 'la':
            self.lemla = True
        elif config['effective_medium'] == 'lll':
            self.lemlll = True
        else:
            print('error in the effective medium')
        
        if config['hoppingmechanism'] == 'ma':
            self.lma = True
        elif config['hoppingmechanism'] == 'marcus':
            self.lmarcus = True
        else:
            print('error in the hopping mechanism')

        if config['np_flavor'] == 'nanoparticle':
            self.nanoparticlesim = True
        elif config['np_flavor'] == 'nanorod':
            self.nanorodsim = True
        else:
            print('error in the nano particle flavor')    

        if config['poisson_solver'] == 'none':
            self.lpoissonnone = True
        elif config['poisson_solver'] == 'nn':
            self.lpoissonnn= True
        elif config['poisson_solver'] == 'ewald':
            self.lpoissonewald= True
        else:
            print('error in the Poisson solver configuration')   
        
        self.pennmodel = False
        
        if config['perx'] == True:
            self.perx = True
        
        if config['pery'] == True:
            self.pery = True
        
        if config['perz'] == True:
            self.perz = True
            
        self.marcusprefac2 = config['marcusprefac2']*c.evtory**2.0
        self.jumpfreq = config['jumpfreq']*c.ry_ps


        self.emass = config['emass'] / 2.0  #I think because of rydberg atomic units
        #[1:42:12 PM] Marton Voros: 2m=1
        #[1:42:24 PM] Marton Voros: http://ilan.schnell-web.net/physics/rydberg.pdf
        self.hmass = config['hmass'] / 2.0
        self.reorgenergy = config['reorgenergy'] * c.evtory
        self.ligandlength = config['ligandlength'] * c.nmtobohr
        self.sourcewf = config['sourcewf'] * c.evtory
        self.drainwf = config['drainwf'] * c.evtory
        self.bradii = config['bradii'] * c.nmtobohr
        self.capacitance = config['capacitance'] * 2.0 / c.evtory
        self.ndist_thr = config['ndist_thr'] * c.nmtobohr
        self.liganddc = config['liganddc']
        self.npdc = config['npdc']
        self.nbnd = config['nbnd']
        self.voltage = 0.15 * c.volt_ry  # voltage in ry
        self.fitting = 1
        
        #config['temperature'] = config['temperature'] * c.kelvintory
        #config['temperature'] = temperature_input * c.kelvintory
        
        #config['voltage'] = config['voltage'] * c.volt_ry
        #config['voltage'] = voltage_input * c.volt_ry
        
        
        
        
        
        
        

    
    
