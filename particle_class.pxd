
#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, initializedcheck = False


import numpy as np
cimport numpy as np
cimport utility as util

ctypedef np.float64_t FLOAT_t
ctypedef np.int_t INT_t



cdef class Nanoparticle:
    cdef INT_t nn
    cdef INT_t[:] nnindex         ###
    cdef FLOAT_t[:] nndistance      #type FLOAT_t
    
    cdef INT_t cn
    cdef INT_t[:] cnindex         ###
    cdef INT_t[:] if_cn
    
    cdef INT_t hcn
    cdef INT_t[:] hcnindex         ###
    cdef INT_t[:] if_hcn

    cdef INT_t cluster         # in which cluster.if cluster = -1, it means it is not in a cluster yet
    cdef bint incluster
    cdef bint edge
    
    cdef FLOAT_t diameter              #
    cdef FLOAT_t radius            #
    cdef FLOAT_t ligandlength       #
    cdef FLOAT_t x
    cdef FLOAT_t y
    cdef FLOAT_t z
    cdef INT_t hotness
       # 
    cdef FLOAT_t[:] cbenergy
    cdef FLOAT_t cbenergy1
    cdef FLOAT_t cbenergy2   
    
    cdef FLOAT_t[:] vbenergy
    cdef FLOAT_t vbenergy1
    cdef FLOAT_t vbenergy2           #
    cdef FLOAT_t selfenergy0         #
    cdef FLOAT_t selfenergy          # 
    cdef FLOAT_t dcin              #cf.config['npdc']   #
    cdef FLOAT_t hselfenergy0            #
    cdef FLOAT_t hselfenergy            #
    cdef bint source  # source 
    cdef bint drain  # drain
    
    cdef INT_t[:] electronindex         #type INT_t
    cdef INT_t[:] orboccupation
    cdef INT_t[:] orbmaxoccupation
    cdef INT_t occupation    # needed for on site energies
    cdef INT_t[:] holeindex
    cdef INT_t[:] vborboccupation 
    cdef INT_t[:] vborbmaxoccupation
    cdef INT_t vboccupation   # needed for on site energies    
    cpdef set_cbenergy1(self)              
    cpdef set_cbenergy2(self)        
    cpdef set_vbenergy(self)
    cpdef set_selfenergy0(self, Sample sample)                                
    cpdef set_selfenergy(self, Sample sample)                    
    cpdef set_holeselfenergy0(self, Sample sample)
    cpdef set_holeselfenergy(self, Sample sample)
   
    
cdef class Electrode:

    cdef INT_t[:] nnindex
    cdef INT_t electronindex 
    cdef INT_t holeindex
    cdef INT_t elecocc
    cdef INT_t holeocc
    cdef INT_t nn
    cdef FLOAT_t energy
    cdef FLOAT_t wf
         # INT_teger :: occ 
         # there can only be one electron waiting
        

cdef class Charge:

    cdef INT_t particleindex
    cdef INT_t orbitalindex
    cdef FLOAT_t charge
    cdef FLOAT_t mass
    cdef FLOAT_t energy
    cdef bint on_electrode 
    cdef INT_t state
    

cdef class Cluster:
    
    cdef INT_t size                       # no. of nanoparticles in the cluster
    cdef np.ndarray npindex                 # indexes of the nanoparticles in the cluster
    #cdef public INT_t[:] zdistance               # distance in z direction for each member
    cdef np.ndarray zdistance               # distance in z direction for each member
    cdef np.ndarray ordered_index           # ordered list of np id 
    cdef np.ndarray reverse_index
    cdef bint has_source 
    cdef bint has_drain 
    cdef bint percolates 

    cpdef set_size(self)
    cpdef find_target(self)
    cpdef find_percolation(self)
       
    
    
cdef class Sample:
    cdef:
        bint lbinarytree 
        bint lheap 
        bint llinearsearch 
        bint lquick 
        bint lcapzunger 
        bint lcapdelerue 
        bint lempnone 
        bint lemmg 
        bint lemla 
        bint lemlll 
        bint lma 
        bint lmarcus 
        bint nanoparticlesim 
        bint nanorodsim 
        bint lpoissonnone 
        bint lpoissonewald 
        bint lpoissonnn 
        bint pennmodel 
        bint lcapacitance0 
        bint perx, pery, perz
        
        FLOAT_t marcusprefac2 
        FLOAT_t jumpfreq 
        FLOAT_t emass
        FLOAT_t hmass 
        FLOAT_t reorgenergy
        FLOAT_t ligandlength 
        FLOAT_t sourcewf 
        FLOAT_t drainwf 
        FLOAT_t bradii 
        FLOAT_t capacitance 
        FLOAT_t temperature    
        FLOAT_t voltage        
        FLOAT_t ndist_thr
        FLOAT_t ediff_thr 
        FLOAT_t dcout 
        INT_t e_degeneracy 
        INT_t h_degeneracy 
        FLOAT_t cellx 
        FLOAT_t celly
        FLOAT_t cellz 
        FLOAT_t cellz_nm 
        FLOAT_t beta 
        INT_t nbnd 
        FLOAT_t packingfraction
        INT_t nelec     # number of electrons
        INT_t nhole     # number of holes
        INT_t nnanops    # number of nanoparticles
        INT_t sample_number   # store the nanoparticle file to open
        FLOAT_t npdc
        FLOAT_t liganddc
        FLOAT_t FWHM
        FLOAT_t fitting
    
    cpdef set_environment(self)
        

        
        
cdef struct s_Charge:
    int particleindex
    int orbitalindex
    double charge
    double mass
    double energy
    int on_electrode 
    int state   
